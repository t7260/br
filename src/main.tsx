import React from 'react'
import ReactDOM from 'react-dom'
import App from './app'

class Boundary extends React.Component<{}, {e: boolean}> {
  constructor(props) {
    super(props);
    this.state = { e: false };
  }

  static getDerivedStateFromError(error) {
      return {e: true}
  }

  render() {
    if (this.state.e) {
      return <p>an error occured</p>
    }

    return this.props.children;
  }
}

ReactDOM.render(
  <Boundary><App /></Boundary>,
  document.getElementById('root')
)
