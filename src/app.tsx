import {Component} from 'react';
import Scene from './c/scene';
import {project} from './p'
import './style.css'

export default class App extends Component<{}, {uuid: string, project: project}> {
  constructor(props: {}) {
    super(props)

    this.state = {
      uuid: '',
      project: null
    }
  }

  componentDidMount() {
    const y = new URLSearchParams(window.location.search).get('u')||''
    this.load(y);
    this.setState({
      uuid: y
    })
  }

  load = (y?) => {
    if(!this.state.uuid&&!y) return;
    fetch('https://api.allorigins.win/raw?url='+encodeURIComponent('https://c.gethopscotch.com/api/v1/projects/'+(y||this.state.uuid)))
    .then(v => v.json())
    .then(v => this.setState({project: v}))
  }

  render() {
    return <>
    <div>
    <input onInput={e => this.setState({uuid: (e.target as HTMLInputElement).value})} value={this.state.uuid} placeholder="uuid" /><button onClick={() => this.load()}>load</button>
    <p>old projects may not work</p>
    </div>
    <div>
    {this.state.project ? this.state.project.scenes.map(v => {
      return <Scene s={v} project={this.state.project} key={v.name} />
    }) : null}
    </div>
    </>
  }
}
