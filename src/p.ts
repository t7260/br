export type project = {
  scenes: scene[],
  objects: obj[],
  rules: rule[],
  customRules: crule[],
  abilities: ability[],
  variables: variable[],
  eventParameters: ep[]
}

export type scene = {
  name: string,
  objects: string[]
}

export type obj = {
  rules: [],
  name: string,
  objectID: string,
  type: number,
  abilityID: string,
  text?: string
}

export type rule = {
  abilityID: string,
  parameters: params[],
  id: string,
  ruleBlockType: number
}

export type crule = {
  id: string,
  rules: string[],
  name: string
}

export type block = {
  type: number,
  parameters: [],
  controlScript?: {
    abilityID: string
  },
  controlFalseScript?: {
    abilityID: string
  }
}

export type ability = {
  abilityID: string,
  blocks: block[],
  name: string
}

export type params = {
  type: number,
  key: string
  value: string,
  datum?: {
    params?: params[],
    type: number,
    variable?: string,
    HSTraitTypeKey?: number,
    HSTraitObjectIDKey?: string,
    HSTraitIDKey?: string,
    HSTraitObjectParameterTypeKey?: number,
    object?: string,
    description?: string
  },
  object?: string,
  variable?: string,
  params?: params[]
}

export type variable = {
  name: string,
  objectIdString: string,
  type: number
}

export type ep = {
  blockType: number,
  description: string,
  id: string,
  objectID?: string
}
