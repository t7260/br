import {Component} from 'react';
import {project} from '../p';
import {objects} from '../blocks'
import Rule from './rule';
import Details from './details'

export default class Object extends Component<{project: project, oid: string}, {pe: string, text: string, rules: string[], o: boolean}> {
  constructor(props) {
    super(props);

    this.state = {
      text: '',
      pe: '',
      rules: [],
      o: false
    }
  }

  componentDidMount() {
    const {text, type, rules} = this.props.project.objects.find(v => v.objectID === this.props.oid)
    this.setState({
      pe: objects[type],
      rules
    })
    if(type === 1) {
      this.setState({
        text
      })
    }
  }

  render() {
    return <div className="parent">
      <div className="name"><Details o={this.state.o} s={(o: boolean) => this.setState({o})} /><span>{this.props.project.objects.find(v => v.objectID === this.props.oid).name}</span><span className="p"><span>{this.state.pe}</span>{this.state.text ? <span className="w" title={this.state.text}>{this.state.text}</span> : null}</span></div>
      {this.state.o ? <div className="child">
      {this.state.rules.map(v => {
        return <Rule key={v} project={this.props.project} rid={v} />
      })}
      </div> : null}
    </div>
  }
}
