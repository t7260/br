import {Component} from 'react';
import {project, scene} from '../p'
import Obj from './object'
import Details from './details'

export default class Scene extends Component<{s: scene, project: project}, {o: boolean}> {
  constructor(props) {
    super(props);

    this.state = {
      o: false
    }
  }

  render() {
    return <div className="parent">
      <div className="name"><Details o={this.state.o} s={(o: boolean) => this.setState({o})} /><span>{this.props.s.name}</span></div>
      {this.state.o ? <div className="child">
      {this.props.s.objects.map(v => {
        return <Obj key={v} oid={v} project={this.props.project} />
      })}
      </div> : null}
    </div>
  }
}
