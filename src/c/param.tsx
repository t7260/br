import {Component, ReactElement} from 'react';
import {params, project} from '../p'
import blocks, {objects} from '../blocks'

export default class Param extends Component<{param: params, project: project}, {string: string, e: ReactElement[]}> {
  constructor(props) {
    super(props)

    if(this.props.param.datum) {
      if(this.props.param.datum.HSTraitIDKey) {
        let s = [<span>{blocks[this.props.param.datum.HSTraitTypeKey][0]}</span>]
        const x = this.props.param.datum.HSTraitObjectIDKey
        if(x) {
          s.push(<span className="w v" title={this.props.project.objects.find(v => v.objectID === x).name}>{this.props.project.objects.find(v => v.objectID === x).name}</span>)
        } else if(this.props.param.datum.HSTraitObjectParameterTypeKey) {
          s.push(<span className="w v" title={blocks[this.props.param.datum.HSTraitObjectParameterTypeKey][0]}>{blocks[this.props.param.datum.HSTraitObjectParameterTypeKey][0]}</span>)
        }

        this.state = {
          string: this.props.param.key,
          e: s
        }
        return;
      }
      let e: ReactElement[]
      if(this.props.param.datum.params) {
        e = this.props.param.datum.params.map((v, i) => <Param project={this.props.project} param={v} key={i} />)
        let st = ''
        if(blocks[this.props.param.datum.type][1] != (this.props.param.datum.params[1]||{key:''}).key) {
          st = blocks[this.props.param.datum.type][1]||this.props.param.key||this.props.param.datum.description
        } else {
          st = this.props.param.key
        }
        this.state = {
          string: st,
          e: e||[]
        }
        return;
      }
      if(this.props.param.datum.variable) {
        let s = []
        let y = false
        if(this.props.param.datum.type && this.props.project.variables.find(v => v.objectIdString === this.props.param.datum.variable).type === 8000) {
          y = true
          s.push(<span className="w">{(this.props.project.objects.find(v => v.objectID === this.props.param.datum.object)||{name: ''}).name||blocks[this.props.param.datum.type][0]}</span>)
        }
        const f = this.props.project.variables.find(v => v.objectIdString === this.props.param.datum.variable)
        s.push(<span className="w v" title={f.name}>{(!y) ? blocks[f.type] : ''} {f.name}</span>)
        this.state = {
          string: this.props.param.key,
          e: s
        }
        return;
      }
      if(this.props.param.type === 54) {
        this.state = {
          string: objects[this.props.param.datum.type],
          e: []
        }
        return;
      }
      this.state = {
        string: (blocks[this.props.param.datum.type]||[null, '?'])[1],
        e: []
      }
    } else {
      if(this.props.param.params) {
        this.state = {
          // @ts-ignore
          string: blocks[this.props.param.type][1],
          e: this.props.param.params.map((v, i) => <Param key={i} project={this.props.project} param={v} />)||[]
        }
        return;
      }

      var e = [<span className="w" title={this.props.param.value}>{this.props.param.value}</span>]
      if(this.props.param.variable) {
        const f = this.props.project.eventParameters.find(v => v.id === this.props.param.variable)
        if(this.props.param.object) {
          e.push(<span className="w">{this.props.project.objects.find(v => v.objectID === this.props.param.object).name}</span>)
        }
        switch(f.blockType) {
          case 8e3:
          e.push(<span>{this.props.project.objects.find(v => v.objectID === f.objectID).name}</span>)
          break;
          case 8003:
          const ii = this.props.project.variables.find(v => v.objectIdString === f.objectID)
          if(!ii) {
            e.push(<span>{f.description}</span>)
          } else {
            e.push(<span>{ii.name}</span>)
          }
          break;
          default:
          e.push(<span>{blocks[f.blockType]||'?'}</span>)
        }
      }

      this.state = {
        string: this.props.param.key,
        e
      }
    }
  }

  componentDidCatch(e, er) {
    console.log(e, er)
  }

  render() {
    return <span className="p">{this.state.string ? <span>{this.state.string}</span> : null}{this.state.e}</span>
  }
}
