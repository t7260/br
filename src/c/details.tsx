import {Component} from 'react';

export default class Details extends Component<{o: boolean, s: Function}> {
  constructor(props) {
    super(props)
  }

  render() {
    return <span style={{marginInlineEnd: '.5em'}} onClick={() => this.props.s(!this.props.o)}>{this.props.o ? <svg fill="none" stroke="currentColor" viewBox="0 0 24 24" xmlns="http://www.w3.org/2000/svg"><path strokeLinecap="round" strokeLinejoin="round" strokeWidth={2} d="M19 9l-7 7-7-7" /></svg> : <svg fill="none" stroke="currentColor" viewBox="0 0 24 24" xmlns="http://www.w3.org/2000/svg"><path strokeLinecap="round" strokeLinejoin="round" strokeWidth={2} d="M9 5l7 7-7 7" /></svg>}</span>
  }
}
