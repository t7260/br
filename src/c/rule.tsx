import React, {Component} from 'react';
import {project, params} from '../p'
import blocks from '../blocks'
import Block from './blocks'
import Param from './param';
import Details from './details'

export default class Rule extends Component<{project: project, rid: string}, {c: boolean, name: string, abil: string, params: params[], o: boolean, t?: string[]}> {
  constructor(props) {
    super(props)

    const x = this.props.project.rules.find(v => v.id === this.props.rid)
    if(!x) {
      const y = this.props.project.customRules.find(v => v.id === this.props.rid);
      this.state = {
        c: true,
        name: y.name,
        abil: '',
        params: [],
        o: false,
        t: y.rules
      }
    } else {
      this.state = {
        name: blocks[x.ruleBlockType][1],
        abil: x.abilityID,
        c: false,
        params: x.parameters,
        o: false
      }
    }
  }

  render() {
    return <div className={`parent rule${this.state.c?' customrule':''}`}>
      <div className="name"><Details s={(o: boolean) => this.setState({o})} o={this.state.o} /><span>{this.state.name}</span><span>{this.state.params.map((v, i) => {
        return <Param project={this.props.project} param={v} key={i} />
      })}</span></div>
      {this.state.o ? <div className="child">{this.state.c ? <>{this.state.t.map((v, i) => <Rule key={i} rid={v} project={this.props.project} />)}</> : <>
      {(this.props.project.abilities.find(v => v.abilityID === this.state.abil)||{blocks: []}).blocks.map((v, i) => {
        return <Block key={i} data={v} project={this.props.project} />
      })}
      </>}</div> : null}
    </div>
  }
}
