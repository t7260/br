import {Component} from 'react';
import blocks from '../blocks';
import {project, block} from '../p';
import style from './blocks.module.css'
import Param from './param'
import Details from './details'

export default class Block extends Component<{project: project, data: block}, {o: boolean}> {
  constructor(props) {
    super(props);

    this.state = {
      o: false
    }
  }

  render() {
    const s = (blocks[this.props.data.type]||['a'])[0]
    const y = (blocks[this.props.data.type]||['a'])[1]

    return <div className={'parent '+style[s]}>
    <div className="name">{this.props.data.controlScript ? <Details o={this.state.o} s={(o) => this.setState({o})} /> : null }<span>{s == 'abl' ? this.props.project.abilities.find(v => v.abilityID === this.props.data.controlScript.abilityID).name : y}</span>{(this.props.data.parameters||[]).map(v => {
      return <Param param={v} project={this.props.project} />
    })}</div>
    {this.state.o ?
    <>{this.props.data.controlScript ? <div className="child">
    {(this.props.project.abilities.find(v => v.abilityID === this.props.data.controlScript.abilityID)||{blocks:[]}).blocks.map((v, i) => {
      return <Block data={v} project={this.props.project} key={i} />
    })}
    </div> : null}
    {this.props.data.controlFalseScript ? <div className="child">
    {((this.props.project.abilities.find(v => v.abilityID === this.props.data.controlFalseScript.abilityID)||{}).blocks||[]).map((v, i) => {
      return <Block data={v} project={this.props.project} key={i} />
    })}
    </div> : null}</> : null}
    </div>
  }
}
